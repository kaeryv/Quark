#pragma once

#include <vector>
#include <functional>
#include <memory>
#include <sstream>

#include "graphics/textures/Texture.hpp"
#include "graphics/Renderable2D.hpp"


#define BIT(x) (1 << x)
