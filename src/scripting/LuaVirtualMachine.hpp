#pragma once

#include <sol.hpp>

namespace quark
{
    class LuaVirtualMachine
    {
        public:
            static std::unique_ptr<sol::state>& LuaState();

        protected:
            static std::unique_ptr<sol::state> Lua;
            static bool Instanciated;
    };
}