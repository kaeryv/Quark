#include "scripting/LuaVirtualMachine.hpp"


namespace quark
{
    std::unique_ptr<sol::state> LuaVirtualMachine::Lua = nullptr;
    bool LuaVirtualMachine::Instanciated = false;
    std::unique_ptr<sol::state>& LuaVirtualMachine::LuaState() 
    {
        if(Instanciated)
        {
            return Lua;
        }
        else
        {
            Lua = std::make_unique<sol::state>();
            Lua->open_libraries(sol::lib::base, sol::lib::math);
            Instanciated = true;
            return Lua;
        }
    }
}