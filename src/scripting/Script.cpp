#include "scripting/Script.hpp"
#include "utils/Log.hpp"
namespace quark
{
    Script::Script(std::string path)
    {
        LuaVirtualMachine::LuaState()->script_file(path);
    }

    void Script::update()
    {
        
    }

    Script::~Script()
    {

    }
}