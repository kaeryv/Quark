#pragma once

#include "scripting/LuaVirtualMachine.hpp"
#include <sol.hpp>
namespace quark
{
    /**
     * Work in progress
     * This class should represent a script attached to an entity
     * I don't know how it will interact yet ...
     * 
     * I'm sure of: 
     *  - It inherits from Component :D
     *  - It is unbreakable, so it is a component.
     *  - It can live in an empty entity without trouble.
     *  - It doesn't care about other components much (maybe overriding some of their behavior but no deps.)
     *  - A script IS NOT a Lua vm, ideally there is only one lua vm.
     *  - 
     */
    class Script
    {
        public:
            Script(std::string path);
            ~Script();

        void update();
        protected:
    };
} // quark
