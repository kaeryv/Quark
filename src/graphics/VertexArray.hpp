#pragma once

#include "VertexBufferLayout.hpp"
#include "VertexBuffer.hpp"

namespace quark
{
    class VertexArray
    {
    public:
        VertexArray();

        ~VertexArray();

        void bind() const;

        void unbind() const;

        void addBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout);

    private:
        unsigned int _renderer;
    };
}
