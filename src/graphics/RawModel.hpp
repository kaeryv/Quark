#pragma once

#include <GL/glew.h>
#include <vector>

#include "VertexBuffer.hpp"
#include "VertexArray.hpp"
#include "IndexBuffer.hpp"

namespace quark
{
    class RawModel
    {
    public:
        RawModel();

        RawModel(VertexArray *va, IndexBuffer *ib);

        ~RawModel();

    public:
        VertexArray *getVaoId();

        std::vector<VertexBuffer *> &getVertexBufferObjects();

        void registerVBO(VertexBuffer *vboID);

        //void setDrawMethod(GLenum drawMethod);

        int getVertexCount();

        void setVertexCount(int vc);

        GLenum getDrawMode();

        inline VertexArray *getVertexArray()
        { return _vaoID; }

        inline IndexBuffer *getIndexBuffer()
        { return _indexBuffer; }


    private:
        std::vector<VertexBuffer *> _vbos;

        VertexArray *_vaoID;
        IndexBuffer *_indexBuffer;

        int _vertexCount;
        GLenum _renderMethod;
    };
}
