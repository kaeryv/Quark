#pragma once

#include <memory>
#include <glm/glm.hpp>
#include "utils/utils.hpp"
#include "graphics/VertexArray.hpp"
#include "graphics/IndexBuffer.hpp"

namespace quark
{
    class Renderable2D
    {
    public:
        Renderable2D();

        Renderable2D(const glm::vec2 &position, const glm::vec2 &size, const glm::vec4 &color);

        virtual ~Renderable2D();


        inline const glm::vec2 &position();

        inline const glm::vec2 &size();

        inline const glm::vec4 &color();

        const VertexArray &vao() const { return *_vao; }

        const IndexBuffer &ibo() const { return *_ibo; }

    protected:
        glm::vec2 _position;
        glm::vec2 _size;
        glm::vec4 _baseColor;

        std::shared_ptr<VertexArray> _vao;
        std::shared_ptr<VertexBuffer> _vbo;
        std::shared_ptr<IndexBuffer> _ibo;

        bool _visible;
    };
} // quark
