#pragma once

#include <GL/glew.h>
#include <memory>
#include "VertexArray.hpp"
#include "IndexBuffer.hpp"
#include "Shader.hpp"
#include "Transform.hpp"

namespace quark
{
    class Window;

    class Renderable2D;

    class Renderer
    {
    public:
        void submit(std::shared_ptr<Renderable2D>);

        void flush();

        //void draw(const VertexArray& va, const IndexBuffer& ib) const;
        void draw(Renderable2D &renderable) const;

        void draw(Renderable2D &renderable, Transform &transform) const;

        void setShader(std::shared_ptr<Shader> s)
        {
          _shader = s;
        }

        void setWindow(Window *w)
        {
          _window = w;
        }

    protected:
        std::shared_ptr<Shader> _shader;
        Window *_window;
    };
}
