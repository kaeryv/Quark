#include "Renderable2D.hpp"


namespace quark
{
    Renderable2D::Renderable2D(const glm::vec2 &position, const glm::vec2 &size,
                               const glm::vec4 &color) :
            _position(position), _size(size), _baseColor(color), _visible(true)
    {
      float positions[] = {
              //  Positions      Texture UVs
              position.x, position.y, 0.0f, 0.0f,
              position.x + size.x, position.y, 1.0f, 0.0f,
              position.x + size.x, position.y + size.y, 1.0f, 1.0f,
              position.x, position.y + size.y, 0.0f, 1.0f
      };

      unsigned int indices[] = {
              0, 1, 2,
              2, 3, 0
      };

      _vao = std::make_shared<VertexArray>();
      _vbo = std::make_shared<VertexBuffer>(positions, 6 * 4 * sizeof(float));

      VertexBufferLayout layout;
      layout.push(GL_FLOAT, 2); // Positions
      layout.push(GL_FLOAT, 2); // Textures
      _vao->addBuffer(*_vbo, layout);

      _ibo = std::make_shared<IndexBuffer>(indices, 6);
    }

    Renderable2D::Renderable2D() :
            Renderable2D(glm::vec2{0, 0}, glm::vec2{100, 100}, glm::vec4{0, 0, 0, 0})
    {

    }

    Renderable2D::~Renderable2D()
    {

    }
}