#include "RawModel.hpp"

namespace quark
{
    RawModel::RawModel()
    {
    }

    RawModel::RawModel(VertexArray *va, IndexBuffer *ib) :
            _vaoID(va),
            _indexBuffer(ib)
    {
    }

    RawModel::~RawModel()
    {
    }

    std::vector<VertexBuffer *> &RawModel::getVertexBufferObjects()
    {
        return _vbos;
    }

    void RawModel::registerVBO(VertexBuffer *vboID)
    {
        _vbos.push_back(vboID);
    }

    VertexArray *RawModel::getVaoId()
    {
        return _vaoID;
    }

    /*void RawModel::setDrawMethod(GLenum drawMethod)
    {
        _renderMethod = drawMethod;
    }*/

    int RawModel::getVertexCount()
    {
        return _vertexCount;
    }

    GLenum RawModel::getDrawMode()
    {
        return _renderMethod;
    }
}
