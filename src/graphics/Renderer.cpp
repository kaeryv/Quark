#include "graphics/Renderer.hpp"
#include "Renderable2D.hpp"
#include "app/Window.hpp"

namespace quark
{
/*
    void Renderer::draw(const VertexArray &va, const IndexBuffer &ib) const
    {
        glm::mat4 proj = glm::ortho(-2.0f, 2.0f, -1.5f, 1.5f, -1.0f, 1.0f);
        glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(-0.2f, 0.0f, 0.0f));
        glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(-0.2f, 0.0f, 0.0f));
        glm::mat4 mvp = proj * view * model;
        _shader->bind();
        _shader->setUniformMat4f("u_MVP", mvp);
        va.bind();
        ib.bind();

        glDrawElements(GL_TRIANGLES, ib.getCount(), GL_UNSIGNED_INT, nullptr);
        ib.unbind();
        va.unbind();
    }
*/

    void Renderer::draw(Renderable2D &renderable) const
    {
      //auto transform = renderable.get_component<Transform>();
      glm::mat4 proj = glm::ortho(
              0.0f, float(_window->getWidth()),
              0.0f, float(_window->getHeight()),
              -1.0f, 1.0f
      );

      glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, -1.0f));

      glm::mat4 model = glm::mat4(1.0f);// transform->getTranslationMatrix();

      glm::mat4 mvp = proj * view * model;
      _shader->bind();
      _shader->setUniformMat4f("u_MVP", mvp);
      renderable.vao().bind();
      renderable.ibo().bind();

      glDrawElements(GL_TRIANGLES, renderable.ibo().getCount(), GL_UNSIGNED_INT, nullptr);

      renderable.ibo().unbind();
      renderable.vao().unbind();
    }

    void Renderer::draw(Renderable2D &renderable, Transform &transform) const
    {
      glm::mat4 proj = glm::ortho(
              0.0f, float(_window->getWidth()),
              0.0f, float(_window->getHeight()),
              -1.0f, 1.0f
      );

      glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, -1.0f));

      glm::mat4 model = transform.getTranslationMatrix();

      glm::mat4 mvp = proj * view * model;
      _shader->bind();
      _shader->setUniformMat4f("u_MVP", mvp);
      renderable.vao().bind();
      renderable.ibo().bind();

      glDrawElements(GL_TRIANGLES, renderable.ibo().getCount(), GL_UNSIGNED_INT, nullptr);

      renderable.ibo().unbind();
      renderable.vao().unbind();
    }

}
