#include "VertexArray.hpp"

namespace quark
{
    VertexArray::VertexArray()
    {
        glGenVertexArrays(1, &_renderer);
    }

    VertexArray::~VertexArray()
    {
        glDeleteVertexArrays(1, &_renderer);
    }

    void VertexArray::bind() const
    {
        glBindVertexArray(_renderer);
    }

    void VertexArray::unbind() const
    {
        glBindVertexArray(0);
    }

    void VertexArray::addBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout)
    {
        bind();
        vb.bind();
        const auto &elements = layout.getElements();

        unsigned int offset = 0;
        for (unsigned int i = 0; i < elements.size(); i++) {
            const auto &element = elements[i];

            glEnableVertexAttribArray(i);
            glVertexAttribPointer(i,
                                  element.count,
                                  element.type,
                                  element.normalized,
                                  layout.getStride(),
                                  (const void *) offset);
            offset += element.count * VertexBufferElement::getTypeSize(element.type);
        }
    }
}
