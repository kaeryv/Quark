#pragma once

#include <GL/glew.h>

#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"

#include <string>

namespace quark
{
    enum class ShaderType
    {
        None = -1, Vertex = 0, Fragment = 1
    };

    struct ShaderSource
    {
        std::string VertexSource;
        std::string FragmentSource;
    };


    class Shader
    {
    public:
        Shader(std::string path);

        virtual ~Shader();

        void init();

        void bind() const;

        void unbind() const;

        void clear();


        void loadInteger(GLint location, int value);

        void loadFloat(GLint location, float value);

        void loadVector(GLint location, glm::vec3 vector);

        void loadBoolean(GLint location, bool value);

        void loadMatrix(GLint location, glm::mat4 value);

        void setUniformMat4f(std::string name, glm::mat4 &matrix);

    protected:
        void bindAttribute(int attribute, std::string variableName);

        virtual void bindAttributes()
        {};

        int getUniformLocation(std::string uniformName);

        virtual void getAllUniformLocations()
        {
            temp = getUniformLocation("u_Texture");
        };
    private:
        void compileShaders();

        void linkShaders();

        GLuint compileShader(std::string source, unsigned type);

    private:
        GLuint _program;
        GLuint _vertexShader;
        GLuint _fragmentShader;

        int temp;

        std::string _sourcePath;
        std::string _programName;
    };
}
