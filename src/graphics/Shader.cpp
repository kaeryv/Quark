#include "graphics/Shader.hpp"

#include "glm/gtc/type_ptr.hpp"
#include "Log.hpp"

#include <fstream>

#define NONE    0

namespace quark
{
    Shader::Shader(std::string path) :
            _sourcePath(path)
    {
    }

    Shader::~Shader()
    {
        clear();
    }

    void Shader::init()
    {
        compileShaders();
        bindAttributes();
        linkShaders();
        getAllUniformLocations();
    }

    void Shader::bind() const
    {
        glUseProgram(_program);
    }

    void Shader::unbind() const
    {
        glUseProgram(NONE);
    }

    void Shader::clear()
    {
        unbind();

        glDetachShader(_program, _vertexShader);
        glDetachShader(_program, _fragmentShader);

        glDeleteShader(_vertexShader);
        glDeleteShader(_fragmentShader);
        glDeleteProgram(_program);
    }

    void Shader::bindAttribute(int attribute, std::string variableName)
    {
        glBindAttribLocation(_program, attribute, variableName.c_str());
    }

    int Shader::getUniformLocation(std::string uniformName)
    {
        int location = glGetUniformLocation(_program, uniformName.c_str());
        Log::Info("Uniform " + uniformName + " at :" + std::to_string(location));
        return location;
    }

    void Shader::loadInteger(GLint location, int value)
    {
        while (GLenum error = glGetError()) {
            Log::Fatal("gl pre-uniform error: " + std::to_string(error));
        }
        //glUniform1i(0, 0);


        while (GLenum error = glGetError()) {
            Log::Fatal("gl uniform error: " + std::to_string(error));
        }

    }

    void Shader::loadFloat(GLint location, float value)
    {
        glUniform1f(location, value);
    }

    void Shader::loadVector(GLint location, glm::vec3 vector)
    {
        glUniform3f(location, vector.x, vector.y, vector.z);
    }

    void Shader::loadBoolean(GLint location, bool value)
    {
        if (value)
            glUniform1f(location, 1.0f);
        else
            glUniform1f(location, 0.0f);
    }

    void Shader::loadMatrix(GLint location, glm::mat4 matrix)
    {
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void Shader::setUniformMat4f(std::string name, glm::mat4 &matrix)
    {
      glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void Shader::linkShaders()
    {
        glLinkProgram(_program);
        glValidateProgram(_program);
    }

    static ShaderSource ParseShader(const std::string &filepath)
    {
        ShaderType type = ShaderType::None;
        std::ifstream stream(filepath, std::ios::in);
        std::stringstream ss[2];
        std::string line;

        while (std::getline(stream, line)) {
            if (line.find("#shader") != std::string::npos) {
                if (line.find("vertex") != std::string::npos)
                    type = ShaderType::Vertex;
                else if (line.find("fragment") != std::string::npos)
                    type = ShaderType::Fragment;
            } else {
                if (type != ShaderType::None)
                    ss[(int) type] << line << "\n";
                else {
                    Log::Warning("Ignored line in shader source [" + filepath + "]");
                    Log::Warning("at: " + line);
                }
            }
        }
        return {ss[0].str(), ss[1].str()};
    }

    void Shader::compileShaders()
    {
        ShaderSource source = ParseShader(_sourcePath);

        Log::Info("Compiling vertex shader from source [" + _sourcePath + "]");
        _vertexShader = compileShader(source.VertexSource, GL_VERTEX_SHADER);

        Log::Info("Compiling fragment shader [" + _sourcePath + "]");
        _fragmentShader = compileShader(source.FragmentSource, GL_FRAGMENT_SHADER);

        _program = glCreateProgram();

        glAttachShader(_program, _vertexShader);
        glAttachShader(_program, _fragmentShader);
    }

    GLuint Shader::compileShader(std::string source, unsigned type)
    {
        GLuint shaderID = glCreateShader(type);
        const char *shaderSource = source.c_str();
        int length = source.length();

        glShaderSource(shaderID, 1, (const GLchar **) &shaderSource, (const GLint *) &length);
        glCompileShader(shaderID);
        GLint shaderCompiled = GL_FALSE;
        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &shaderCompiled);

        if (shaderCompiled == GL_FALSE) {
            GLchar *info = new GLchar[300];
            int infosize = 0;
            glGetShaderInfoLog(shaderID, 300, &infosize, info);
            Log::Info(info);
            Log::Info(std::to_string(infosize));
            glDeleteShader(shaderID);
            Log::Fatal("Failed to compile shader [" + _sourcePath + "]");

        }
        return shaderID;
    }
}
