#pragma once

#include "core.h"
#include "glm.hpp"

namespace quark
{
    class Sprite : public Renderable2D
    {
        public:
            Sprite(std::shared_ptr<Texture>);
            Sprite(float x, float y, float w, float h, std::shared_ptr<Texture>);
            Sprite(float x, float y, float w, float h, glm::vec4 color);
            ~Sprite();
    };
}