#pragma once

#include "graphics/Renderer.hpp"

namespace quark
{
  class Texture
  {
    public:
      Texture(const std::string& path);
      
      ~Texture();
      
      void bind(unsigned int slot = 0) const;
      
      void unbind() const;
      
      inline int getWidth() const
      {
        return _width;
      }
      
      inline int getHeight() const
      {
        return _height;
      }
      
    private:
      unsigned int _renderer;
      std::string _filePath;
      unsigned char* _localBuffer;
      int _width;
      int _height;
      int _bpp;
  };
  
  class DynamicTexture
  {
    public:
      DynamicTexture(unsigned char* dyndata,
                     const int width,
                     const int height,
                     const int bpp);
                     
      ~DynamicTexture();
      
      void bind(unsigned int slot = 0) const;
      
      void unbind() const;
      
      void update(unsigned char* dyndata);
      
      inline int getWidth() const
      {
        return _width;
      }
      
      inline int getHeight() const
      {
        return _height;
      }
      
    private:
      unsigned int _renderer;
      std::string _filePath;
      unsigned char* _localBuffer;
      int _width;
      int _height;
      int _bpp;
  };
}
