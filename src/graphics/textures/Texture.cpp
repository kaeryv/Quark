#include "graphics/textures/Texture.hpp"
#define STB_IMAGE_IMPLEMENTATION

#include <vendor/stb/stb_image.h>
#include "utils/Log.hpp"

namespace quark
{
  Texture::Texture(const std::string& path) :
    _filePath(path),
    _localBuffer(nullptr),
    _width(0),
    _height(0)
  {
    QK_CORE_INFO("Loading texture [{0}]", path);
    stbi_set_flip_vertically_on_load(1);
    _localBuffer = stbi_load(path.c_str(), &_width, &_height, &_bpp, 4);
    
    QK_CORE_TRACE("Generating OpenGL texture handle.");
    glGenTextures(1, &_renderer);
    glBindTexture(GL_TEXTURE_2D, _renderer);
    
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _width, _height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, _localBuffer);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    if (_localBuffer) {
      stbi_image_free(_localBuffer);
      QK_CORE_TRACE("Freed texture local buffer.");
    }
    
    QK_CORE_WARN("Loaded texture is {0} by {1} with {2} bpp.", _width, _height,
                 _bpp);
  }
  
  Texture::~Texture()
  {
    glDeleteTextures(1, &_renderer);
  }
  
  void Texture::bind(unsigned int slot) const
  {
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, _renderer);
    QK_CORE_TRACE("Bound texture in slot {0}", slot);
  }
  
  void Texture::unbind() const
  {
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  
  
  DynamicTexture::DynamicTexture(unsigned char* data, const int width,
                                 const int height, int bpp) :
    _localBuffer(nullptr),
    _width(width),
    _height(height),
    _bpp(bpp)
  {
  
    glGenTextures(1, &_renderer);
    glBindTexture(GL_TEXTURE_2D, _renderer);
    
    unsigned char* temp = new unsigned char[height * width * 4];
    
    for (int i = 0; i < height * width; i++) {
      temp[i * 4] = 255 * data[height * width - i];
      temp[i * 4 + 1] = 0;
      temp[i * 4 + 2] = 0;
      temp[i * 4 + 3] = 255;
    }
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _width, _height, 0, GL_RED,
                 GL_UNSIGNED_BYTE, data);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    delete temp;
    DEBUG("width " << _width << " height " << _height << " bbp " << _bpp);
  }
  
  DynamicTexture::~DynamicTexture()
  {
    glDeleteTextures(1, &_renderer);
  }
  
  void DynamicTexture::update(unsigned char* data)
  {
    glBindTexture(GL_TEXTURE_2D, _renderer);
    
    unsigned char temp[64 * 32 * 4];
    
    for (int i = 0; i < _height; i++) {
      for (int j = 0; j < _width; j++) {
        temp[(j) + _width * (_height - i - 1)] = 255 * data[i * _width + j];
      }
    }
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _width, _height, 0, GL_RED,
                 GL_UNSIGNED_BYTE, temp);
    //glBindTexture(GL_TEXTURE_2D, 0);
    bind();
  }
  
  void DynamicTexture::bind(unsigned int slot) const
  {
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, _renderer);
    
    while (GLenum error = glGetError()) {
      Log::Fatal("gl texture error: " + std::to_string(error));
    }
  }
  
  void DynamicTexture::unbind() const
  {
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  
}


