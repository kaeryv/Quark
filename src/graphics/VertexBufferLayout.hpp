#pragma once

#include <GL/glew.h>
#include <vector>
#include "Log.hpp"

namespace quark
{
    struct VertexBufferElement
    {
        unsigned int type;
        unsigned int count;
        unsigned int normalized;

        static unsigned int getTypeSize(unsigned int type)
        {
            switch (type) {
                case GL_FLOAT:
                    return sizeof(GLfloat);
                case GL_BYTE:
                    return sizeof(GLbyte);
                case GL_UNSIGNED_INT:
                    return sizeof(GLuint);
                case GL_INT:
                    return sizeof(GLint);
            }
            Log::Fatal("Unknown GL type.");
            return 0;
        }
    };

    class VertexBufferLayout
    {
    public:
        VertexBufferLayout() :
                _stride(0)
        {
        }

        ~VertexBufferLayout()
        {}

        void push(unsigned int type, unsigned int count)
        {
            _elements.push_back({type, count, GL_FALSE});
            _stride += count * VertexBufferElement::getTypeSize(type);
        }

        inline unsigned int getStride() const
        { return _stride; }

        inline const std::vector<VertexBufferElement> &getElements() const
        { return _elements; }

    private:
        std::vector<VertexBufferElement> _elements;
        unsigned int _stride;
    };

}
