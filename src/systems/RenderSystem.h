//
// Created by kaeryv on 9/12/18.
//

#ifndef SANDBOX_RENDERSYSTEM_H
#define SANDBOX_RENDERSYSTEM_H

#include <entityx/entityx.h>
#include <graphics/Transform.hpp>
#include "graphics/Renderer.hpp"
#include "graphics/Renderable2D.hpp"

namespace ex = entityx;

namespace quark
{
    class RenderSystem : public ex::System<RenderSystem>
    {
    public:
        explicit RenderSystem(Renderer *target)
        {
          _target = target;
        }

        void update(ex::EntityManager &es, ex::EventManager &events, ex::TimeDelta dt)
        {
          int c = 0;
          es.each<Renderable2D>([&](ex::Entity entity, Renderable2D &) { ++c; });

          for (int i = 0; i < 5 - c; i++) {
            ex::Entity entity = es.create();
            entity.assign<Renderable2D>();
            entity.assign<Transform>();
          }

          int i = 0;
          es.each<Renderable2D, Transform>(
                  [&, this](ex::Entity entity, Renderable2D &renderable, Transform &transform) {
                      _target->draw(renderable, transform);
                      transform.update(++i);
                  });
        }

    protected:
        Renderer *_target;
    };
}

#endif //SANDBOX_RENDERSYSTEM_H
