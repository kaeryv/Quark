#pragma once




namespace quark
{
    template<typename T>
    class Time
    {
        public:
            Time(float value);
            ~Time();
    };

    class TimeInterval
    {
        public:
            TimeInterval();
            ~TimeInterval();
    };
} // quark
