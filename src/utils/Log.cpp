#include "Log.hpp"

#include "spdlog/sinks/stdout_color_sinks.h"
#include "scripting/LuaVirtualMachine.hpp"

namespace quark {

	std::shared_ptr<spdlog::logger> Loggy::CoreLogger;
	std::shared_ptr<spdlog::logger> Loggy::ClientLogger;
	std::shared_ptr<spdlog::logger> Loggy::ScriptLogger;

	void Loggy::Init()
	{
		//spdlog::set_pattern("%^[%T] %n: %v%$");
		CoreLogger = spdlog::stdout_color_mt("QUARK");
		CoreLogger->set_level(spdlog::level::trace);

		ClientLogger = spdlog::stdout_color_mt("APP");
		ClientLogger->set_level(spdlog::level::trace);

		ScriptLogger = spdlog::stdout_color_mt("LUA");
		ScriptLogger->set_level(spdlog::level::trace);

		/**
		 * Setup Lua integration
		 * Temporary setup
		 */
		LuaVirtualMachine::LuaState()->set_function(
			"LOG_TRACE",
			[](std::string str){GetScriptLogger()->trace(str); }
		);
		LuaVirtualMachine::LuaState()->set_function(
			"LOG_INFO",
			[](std::string str){GetScriptLogger()->info(str); }
		);
		LuaVirtualMachine::LuaState()->set_function(
			"LOG_WARN",
			[](std::string str){GetScriptLogger()->warn(str); }
		);
		LuaVirtualMachine::LuaState()->set_function(
			"LOG_ERROR",
			[](std::string str){GetScriptLogger()->error(str); }
		);
	}

}