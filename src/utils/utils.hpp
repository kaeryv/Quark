#pragma once

#include <chrono>
#include <filesystem>

typedef std::filesystem::file_time_type std_time_t;

enum class Unit : int {
  s, ms
};


template<Unit U>
static float FrameTimeFromFrequency(float fps)
{
  return 1.0f / fps;
}

template<>
float FrameTimeFromFrequency<Unit::ms>(float fps)
{
  return 1000.0f / fps;
}


template<>
float FrameTimeFromFrequency<Unit::s>(float fps)
{
  return 1.0f / fps;
}


class Timer
{
  public:
    Timer() : m_Accumulator(0.0f)
    {
    
    }
    
    inline void start()
    {
      m_Enabled = true;
      std_time_t start = std::chrono::system_clock::now();
      m_LastTimeStamp = start;
    }
    
    inline void stop()
    {
      m_Enabled = false;
    }
    
    inline void update()
    {
      std_time_t time = std::chrono::system_clock::now();
      m_Accumulator += (time - m_LastTimeStamp);
      m_LastTimeStamp = time;
    }
    
    inline void softReset()
    {
      m_Accumulator = std::chrono::duration<float>::zero();
      this->update();
    }
    
    inline float getCurrentValue()
    {
      return m_Accumulator.count();
    }
    
    ~Timer()
    {
    
    }
  private:
    bool m_Enabled;
    std::chrono::duration<float> m_Accumulator;
    std_time_t m_LastTimeStamp;
};


class Color
{
  public:
    Color(float r, float g, float b, float a):
		r(r),g(g),b(b),a(a) 
	{}
    ~Color() {}
    float r;
    float g;
    float b;
    float a;
};