#pragma once

#include "core.h"
#include <spdlog/spdlog.h>
#include "spdlog/fmt/ostr.h"

namespace quark 
{
	class Loggy
	{
	public:
		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return ClientLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetScriptLogger() { return ScriptLogger; }
	private:
		static std::shared_ptr<spdlog::logger> CoreLogger;
		static std::shared_ptr<spdlog::logger> ClientLogger;
		static std::shared_ptr<spdlog::logger> ScriptLogger;
	};
}

// Core log macros
#define QK_CORE_TRACE(...)    ::quark::Loggy::GetCoreLogger()->trace(__VA_ARGS__)
#define QK_CORE_INFO(...)     ::quark::Loggy::GetCoreLogger()->info(__VA_ARGS__)
#define QK_CORE_WARN(...)     ::quark::Loggy::GetCoreLogger()->warn(__VA_ARGS__)
#define QK_CORE_ERROR(...)    ::quark::Loggy::GetCoreLogger()->error(__VA_ARGS__)
#define QK_CORE_FATAL(...)    ::quark::Loggy::GetCoreLogger()->critical(__VA_ARGS__);exit(-1)

// Client log macros
#define QK_TRACE(...)	      ::quark::Loggy::GetClientLogger()->trace(__VA_ARGS__)
#define QK_INFO(...)	      ::quark::Loggy::GetClientLogger()->info(__VA_ARGS__)
#define QK_WARN(...)	      ::quark::Loggy::GetClientLogger()->warn(__VA_ARGS__)
#define QK_ERROR(...)	      ::quark::Loggy::GetClientLogger()->error(__VA_ARGS__)
#define QK_FATAL(...)	      ::quark::Loggy::GetClientLogger()->critical(__VA_ARGS__); exit(-1)