#pragma once

#include "core.h"
#include "Event.h"


namespace quark
{
  typedef int KeyCode;
  class KeyEvent : public Event
  {
    public:
      inline KeyCode keyCode() const { return _keycode; }

      EVENT_CLASS_CATEGORY(EventCategoryKeyboard | EventCategoryInput)
  
    protected:
      KeyEvent(int keycode):
        _keycode(keycode) {}
      KeyCode _keycode;    
  };

  class KeyPressedEvent : public KeyEvent
  {
    public:
      KeyPressedEvent(KeyCode key, int repeat):
        KeyEvent(key), _repeatCount(repeat) {}

      inline int numRepeat() const { return _repeatCount; }

      std::string string() const override
      {
        std::stringstream ss;
        ss << "KeyPressedEvent: " << _keycode << " (" << _repeatCount << " repeats)";
        return ss.str();
      }

      EVENT_CLASS_TYPE(KeyPressed)
    private:
        int _repeatCount;
  };

  class KeyReleaseEvent : public KeyEvent
  {
    public:
      KeyReleaseEvent(KeyCode key):
        KeyEvent(key) {}

      std::string string() const override
      {
        std::stringstream ss;
        ss << "KeyReleasedEvent: " << _keycode;
        return ss.str();
      }
      EVENT_CLASS_TYPE(KeyReleased)
  };
}
