//
// Created by kaeryv on 27/05/18.
//

#include "InputManager.h"


namespace quark
{
    InputManager *Input::_inputManager = nullptr;

    InputManager::InputManager()
    {
        clearKeys();

        Input::_inputManager = this;
    }

    void InputManager::clearKeys()
    {
        for (unsigned int i = 0; i < MAX_KEYS; i++) {
            _lastKeyState[i] = false;
            _keyState[i] = false;
        }
        _keyModifiers = 0;

    }

    InputManager::~InputManager()
    {

    }

    void InputManager::update()
    {
        memcpy(_lastKeyState, _keyState, MAX_KEYS);
    }

    bool InputManager::isKeyDown(unsigned int keycode) const
    {
        if (keycode > MAX_KEYS)
            return false;
        return _keyState[keycode];
    }

    void Input::glfwKeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
    {
        if (action == GLFW_PRESS) {
            _inputManager->setKeyState(key, true);
        } else if (action == GLFW_RELEASE) {
            _inputManager->setKeyState(key, false);
        }
    }
}


