#pragma once

#include "core.h"
#include <string>
#include <functional>

namespace quark
{
  /*
   * The type of the event, sorted by categories
   */

  enum class EventType
  {
    None = 0,
    WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
    AppTick, AppUpdate, AppRender,
    KeyPressed, KeyReleased,
    MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
  };

  /*
   * The events' categories used for sorting when dispatching
   */

  enum EventCategory
  {
    EventCategoryNone = 0,
    EventCategoryApplication = BIT(0),
    EventCategoryInput       = BIT(1),
    EventCategoryKeyboard    = BIT(2),
    EventCategoryMouse       = BIT(3),
    EventCategoryMouseButton = BIT(4)
  };


#define EVENT_CLASS_TYPE(classtype) static EventType GetStaticType() { return EventType::classtype; } \
  virtual EventType eventType() const override { return GetStaticType(); } \
  virtual const char* name() const override { return #classtype; }

#define EVENT_CLASS_CATEGORY(category) virtual int categoryFlags() const override { \
  return category; }
  
  class Event
  {
    friend class EventDispatcher;
    
    public:

      virtual EventType eventType() const = 0;
      virtual const char* name() const = 0;
      virtual int categoryFlags() const = 0;
      virtual std::string string() const { return name(); }

      inline bool IsInCategory(int category)
      {
        return categoryFlags() & category;
      }

    protected:
      bool _handled = false;
  };

  class EventDispatcher
  {
    template<typename T>
    using EventFn = std::function<bool(T&)>;
    
    public:
      
      EventDispatcher(Event& event):
        _event(event)
      {
    
      }

      template<typename T>
      bool dispatch(EventFn<T> func)
      {
        if(_event.eventType() == T::GetStaticType)
        {
          _event._handled = func(*(T*)&_event);
          return true;
        }
        return false;
      }
    private:
      Event& _event;
  };

  inline std::ostream& operator<<(std::ostream& os, const Event& event)
  {
    return os << event.string();
  }
}
