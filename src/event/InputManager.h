#pragma once

#include <functional>
#include "app/Window.hpp"

#define MAX_KEYS    1024
#define MAX_BUTTONS 32

#include "Log.hpp"


#include "scripting/LuaVirtualMachine.hpp"

namespace quark
{
    typedef std::function<void(void)> WindowEventCallback;

    class InputManager
    {
    private:
        friend class Window;

    protected:
        bool _keyState[MAX_KEYS];
        bool _lastKeyState[MAX_KEYS];

        int32_t _keyModifiers;

        WindowEventCallback _eventCallBack;
    public:
        InputManager();

        ~InputManager();

        inline void setEventCallBack(const WindowEventCallback &eventCallback)
        {
          _eventCallBack = eventCallback;
        }

        inline void setKeyState(unsigned int key, bool state)
        {
          _keyState[key] = state;
        }

        void update();

        bool isKeyDown(unsigned int keycode) const;

        void clearKeys();

    };

    class Input
    {
    private:
        friend class InputManager;

    private:
        static InputManager *_inputManager;

    public:
        inline static void Init()
        {
          _inputManager = new InputManager();
          _inputManager->clearKeys();

          LuaVirtualMachine::LuaState()->new_usertype<Input>(
                  "Input",
                  "key_down",
                  &Input::isKeyDown);
        }

        inline static bool isKeyDown(unsigned int keycode)
        {
          return _inputManager->isKeyDown(keycode);
        }

        inline static InputManager *getInputManager()
        {
          return _inputManager;
        }

        static void glfwKeyCallback(GLFWwindow *window, int key, int scancode,
                                    int action, int mods);
    };

#define QK_MOUSE_LEFT      0x00
#define QK_MOUSE_MIDDLE      0x01
#define QK_MOUSE_RIGHT    0x02

#define QK_NO_CURSOR      NULL

#define QK_MODIFIER_LEFT_CONTROL    BIT(0)
#define QK_MODIFIER_LEFT_ALT        BIT(1)
#define QK_MODIFIER_LEFT_SHIFT        BIT(2)
#define QK_MODIFIER_RIGHT_CONTROL    BIT(3)
#define QK_MODIFIER_RIGHT_ALT        BIT(4)
#define QK_MODIFIER_RIGHT_SHIFT        BIT(5)

#define QK_KEY_TAB              0x09

#define QK_KEY_0              0x30
#define QK_KEY_1              0x31
#define QK_KEY_2              0x32
#define QK_KEY_3              0x33
#define QK_KEY_4              0x34
#define QK_KEY_5              0x35
#define QK_KEY_6              0x36
#define QK_KEY_7              0x37
#define QK_KEY_8              0x38
#define QK_KEY_9              0x39

#define QK_KEY_A              0x41
#define QK_KEY_B              0x42
#define QK_KEY_C              0x43
#define QK_KEY_D              0x44
#define QK_KEY_E              0x45
#define QK_KEY_F              0x46
#define QK_KEY_G              0x47
#define QK_KEY_H              0x48
#define QK_KEY_I              0x49
#define QK_KEY_J              0x4A
#define QK_KEY_K              0x4B
#define QK_KEY_L              0x4C
#define QK_KEY_M              0x4D
#define QK_KEY_N              0x4E
#define QK_KEY_O              0x4F
#define QK_KEY_P              0x50
#define QK_KEY_Q              0x51
#define QK_KEY_R              0x52
#define QK_KEY_S              0x53
#define QK_KEY_T              0x54
#define QK_KEY_U              0x55
#define QK_KEY_V              0x56
#define QK_KEY_W              0x57
#define QK_KEY_X              0x58
#define QK_KEY_Y              0x59
#define QK_KEY_Z              0x5A

#define QK_KEY_LBUTTON        0x01
#define QK_KEY_RBUTTON        0x02
#define QK_KEY_CANCEL         0x03
#define QK_KEY_MBUTTON        0x04

#define QK_KEY_ESCAPE         0x1B
#define QK_KEY_SHIFT          0x10
#define QK_KEY_CONTROL        0x11
#define QK_KEY_MENU           0x12
#define QK_KEY_ALT              QK_KEY_MENU
#define QK_KEY_PAUSE          0x13
#define QK_KEY_CAPITAL        0x14

#define QK_KEY_QKACE          0x20
#define QK_KEY_PRIOR          0x21
#define QK_KEY_NEXT           0x22
#define QK_KEY_END            0x23
#define QK_KEY_HOME           0x24
#define QK_KEY_LEFT           0x25
#define QK_KEY_UP             0x26
#define QK_KEY_RIGHT          0x27
#define QK_KEY_DOWN           0x28
#define QK_KEY_SELECT         0x29
#define QK_KEY_PRINT          0x2A
#define QK_KEY_EXECUTE        0x2B
#define QK_KEY_SNAPSHOT       0x2C
#define QK_KEY_INSERT         0x2D
#define QK_KEY_DELETE         0x2E
#define QK_KEY_HELP           0x2F

#define QK_KEY_NUMPAD0        0x60
#define QK_KEY_NUMPAD1        0x61
#define QK_KEY_NUMPAD2        0x62
#define QK_KEY_NUMPAD3        0x63
#define QK_KEY_NUMPAD4        0x64
#define QK_KEY_NUMPAD5        0x65
#define QK_KEY_NUMPAD6        0x66
#define QK_KEY_NUMPAD7        0x67
#define QK_KEY_NUMPAD8        0x68
#define QK_KEY_NUMPAD9        0x69
#define QK_KEY_MULTIPLY       0x6A
#define QK_KEY_ADD            0x6B
#define QK_KEY_SEPARATOR      0x6C
#define QK_KEY_SUBTRACT       0x6D
#define QK_KEY_DECIMAL        0x6E
#define QK_KEY_DIVIDE         0x6F
#define QK_KEY_F1             0x70
#define QK_KEY_F2             0x71
#define QK_KEY_F3             0x72
#define QK_KEY_F4             0x73
#define QK_KEY_F5             0x74
#define QK_KEY_F6             0x75
#define QK_KEY_F7             0x76
#define QK_KEY_F8             0x77
#define QK_KEY_F9             0x78
#define QK_KEY_F10            0x79
#define QK_KEY_F11            0x7A
#define QK_KEY_F12            0x7B
#define QK_KEY_F13            0x7C
#define QK_KEY_F14            0x7D
#define QK_KEY_F15            0x7E
#define QK_KEY_F16            0x7F
#define QK_KEY_F17            0x80
#define QK_KEY_F18            0x81
#define QK_KEY_F19            0x82
#define QK_KEY_F20            0x83
#define QK_KEY_F21            0x84
#define QK_KEY_F22            0x85
#define QK_KEY_F23            0x86
#define QK_KEY_F24            0x87

#define QK_KEY_NUMLOCK        0x90
#define QK_KEY_SCROLL         0x91

#define QK_KEY_LSHIFT         0xA0
#define QK_KEY_RSHIFT         0xA1
#define QK_KEY_LCONTROL       0xA2
#define QK_KEY_RCONTROL       0xA3
#define QK_KEY_LMENU          0xA4
#define QK_KEY_RMENU          0xA5
}


