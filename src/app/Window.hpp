#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

#include <imgui.h>

#include "vendor/imgui_impl_glfw_gl3.h"

#include <map>

#include "glm/vec2.hpp"

#include "scripting/LuaVirtualMachine.hpp"


namespace quark
{

    static void GLFWKeyCallback(GLFWwindow *window,
                                int key,
                                int scancode,
                                int action,
                                int mods);

    static void GLFWMouseButtonCallback(GLFWwindow *glfwWindow,
                                        int button,
                                        int action,
                                        int mods);

    static void GLFWMousePositionCallback(GLFWwindow *glfwWindow,
                                          double xpos,
                                          double ypos);

    static void GLFWWindowSizeCallback(GLFWwindow *window,
                                       int width,
                                       int height);

    struct WindowProperties
    {
        unsigned int width;
        unsigned int height;

        bool fullscreen;
        bool vsync;
    };

    class Window
    {
    public:
        Window();

        Window(const std::string &title, const WindowProperties &props);

        ~Window();

    public:
        void update();

        void clear();

        void clear() const;

        bool isClosed() const
        {
          return _closed;
        }

        void setClearColor(float r, float g, float b);
      
    public:
        inline unsigned getWidth() const
        {
          return _properties.width;
        }

        inline unsigned getHeight() const
        {
          return _properties.height;
        }

        inline bool isVsync() const
        {
          return _properties.vsync;
        }

        void setTitle(const std::string &title)
        {
          _title = title;
        }

        WindowProperties &properties()
        {
          return _properties;
        }

        void setVsync(bool vsync);

        static void InitLuaBindings()
        {
          LuaVirtualMachine::LuaState()->new_usertype<Window>(
                  "Window",
                  "setClearColor", &Window::setClearColor,
                  "title", &Window::_title);

        }
    private:
        WindowProperties _properties;
        std::string _title;

        bool _closed;

        GLFWwindow *_window;

    private:
        static std::map<void *, Window *> _handles;

    };

}

