#include "Window.hpp"

#include "utils/Log.hpp"
#include"event/InputManager.h"



#define GL_ERROR_ENTRY(entry) case entry: return #entry;

static const char* GetGlErrorString(GLenum type)
{
  switch (type) {
    GL_ERROR_ENTRY(GL_NO_ERROR)
    GL_ERROR_ENTRY(GL_INVALID_ENUM)
    GL_ERROR_ENTRY(GL_INVALID_VALUE)
    GL_ERROR_ENTRY(GL_INVALID_OPERATION)
    GL_ERROR_ENTRY(GL_INVALID_FRAMEBUFFER_OPERATION)
    GL_ERROR_ENTRY(GL_OUT_OF_MEMORY)
    GL_ERROR_ENTRY(GL_STACK_OVERFLOW)
    GL_ERROR_ENTRY(GL_STACK_UNDERFLOW)

    default:
      return "Unknown error string";
  }
}

static std::string GetGlErrorSourceString(GLenum source)
{
  switch (source) {
    GL_ERROR_ENTRY(GL_DEBUG_SOURCE_API)
    GL_ERROR_ENTRY(GL_DEBUG_SOURCE_WINDOW_SYSTEM)
    GL_ERROR_ENTRY(GL_DEBUG_SOURCE_SHADER_COMPILER)
    GL_ERROR_ENTRY(GL_DEBUG_SOURCE_THIRD_PARTY)
    GL_ERROR_ENTRY(GL_DEBUG_SOURCE_APPLICATION)
    GL_ERROR_ENTRY(GL_DEBUG_SOURCE_OTHER)

    default:
      return "Unknown error source";
  }
}


namespace quark
{
    void GLAPIENTRY openglCallbackFunction(
            GLenum source,
            GLenum type,
            GLuint id,
            GLenum severity,
            GLsizei length,
            const GLchar *message,
            const void *userParam)
    {
      QK_CORE_TRACE("From {0}", GetGlErrorSourceString(source));

      switch (severity) {
        case GL_DEBUG_SEVERITY_NOTIFICATION:
          QK_CORE_TRACE(message);
          break;

        case GL_DEBUG_SEVERITY_LOW:
          QK_CORE_WARN(message);
          break;

        case GL_DEBUG_SEVERITY_MEDIUM:
          QK_CORE_ERROR(message);
          break;

        case GL_DEBUG_SEVERITY_HIGH:
        QK_CORE_FATAL(message);
          break;
      }
    }

    std::map<void *, Window *> Window::_handles;

    Window::Window()
    {
    }

    Window::Window(const std::string &title, const WindowProperties &props) :
            _closed(false),
            _title(title),
            _properties(props)
    {
      if (!glfwInit()) {
        QK_CORE_FATAL("Failed to init glfw");
      }

      glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
      glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);


      _window = glfwCreateWindow(props.width, props.height, title.c_str(), NULL,
                                 NULL);

      if (_window == nullptr) {
        glfwTerminate();
        QK_CORE_FATAL("Failed to create window.");
      }

      glfwMakeContextCurrent(_window);

      if (glewInit() != GLEW_OK) {
        QK_CORE_FATAL("Failed to init glew.");
      }

      glfwSetWindowUserPointer(_window, this);
      glfwSetKeyCallback(_window, GLFWKeyCallback);
      glfwSetMouseButtonCallback(_window, GLFWMouseButtonCallback);
      glfwSetCursorPosCallback(_window, GLFWMousePositionCallback);
      glfwSetWindowSizeCallback(_window, GLFWWindowSizeCallback);

      QK_CORE_INFO("Enabling OpenGL synchronous debugging.");
      glEnable(GL_DEBUG_OUTPUT);
      glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
      QK_CORE_TRACE("Registering OpenGL debug callback");
      glDebugMessageCallback(openglCallbackFunction, nullptr);
      GLuint unusedIds = 0;
      glDebugMessageControl(
              GL_DONT_CARE,
              GL_DONT_CARE,
              GL_DONT_CARE,
              0,
              &unusedIds,
              true);


      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      QK_CORE_INFO("Initiating IMGUI");
      ImGui::CreateContext();
      ImGui_ImplGlfwGL3_Init(_window, true);

      ImGui::StyleColorsDark();

      QK_CORE_INFO("Init input system");
      Input::Init();
      QK_CORE_TRACE("Set input callback");
      glfwSetKeyCallback(_window, Input::glfwKeyCallback);

      // Export object to lua
      (*LuaVirtualMachine::LuaState())["MainWindow"] = &(*this);

      // Export window debug overlay to lua
      LuaVirtualMachine::LuaState()->set_function(
              "IG_TEXT",
              ImGui::Text
      );
      LuaVirtualMachine::LuaState()->set_function(
              "IG_BWIN",
              [](const char *s) {
                  ImGui::Begin(s);
              }
      );
      LuaVirtualMachine::LuaState()->set_function(
              "IG_EWIN",
              []() {
                  ImGui::End();
              }
      );
    }

    Window::~Window()
    {
      glfwTerminate();
    }

    void Window::clear()
    {
      glClear(GL_COLOR_BUFFER_BIT);
      ImGui_ImplGlfwGL3_NewFrame();
    }

    void Window::update()
    {
      _closed = glfwWindowShouldClose(_window);

      glfwSwapBuffers(_window);
      glfwPollEvents();
    }

    void Window::setVsync(bool vsync)
    {

    }

    void Window::setClearColor(float r, float g, float b)
    {
      glClearColor(r, b, g, 1.0);
    }


    static void GLFWKeyCallback(GLFWwindow *glfwWindow, int key, int scancode,
                                int action, int mods)
    {
      /*
      Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);
      if (action == GLFW_PRESS)
       _window->DispatchEvent(KeyPressedEvent(key, 0));
      else if (action == GLFW_REPEAT)
       window->DispatchEvent(KeyPressedEvent(key, 1));
      else if (action == GLFW_RELEASE)
       window->DispatchEvent(KeyReleasedEvent(key));

      window->m_InputManager->m_KeyState[key] = (action == GLFW_PRESS) || (action == GLFW_REPEAT);
      */
    }

    static void GLFWMouseButtonCallback(GLFWwindow *glfwWindow, int button,
                                        int action, int mods)
    {
      /*
      Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);
      if (action == GLFW_PRESS)
       window->DispatchEvent(MouseButtonPressedEvent(button, 0));
      else if (action == GLFW_REPEAT)
       window->DispatchEvent(MouseButtonPressedEvent(button, 1));
      else if (action == GLFW_RELEASE)
       window->DispatchEvent(MouseButtonReleasedEvent(button));

      window->m_InputManager->m_MouseButtons[button] = (action == GLFW_PRESS) || (action == GLFW_REPEAT);
      */
    }

    static void GLFWMousePositionCallback(GLFWwindow *glfwWindow, double xpos,
                                          double ypos)
    {
      /*
      Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);
      window->DispatchEvent(MouseMovedEvent((float)xpos, (float)ypos, 0, 0));

      window->m_InputManager->m_MousePosition = math::vec2((float)xpos, (float)ypos);
      */
    }

    static void GLFWWindowSizeCallback(GLFWwindow *glfwWindow, int width,
                                       int height)
    {
      Window *window = (Window *) glfwGetWindowUserPointer(glfwWindow);
      window->properties().width = width;
      window->properties().height = height;
      QK_CORE_TRACE("Window resized, {0} x {1}", width, height);
      //window->m_ResizeCallback(width, height);
      glViewport(0, 0, width, height);
    }
}

