#include "Application.hpp"

#include <thread>
#include <deque>
#include "utils/Log.hpp"
#include "scripting/Script.hpp"

namespace quark
{
    Application::Application() : m_GUI_MaxFramesPerSecond(60), m_MaxTicksPerSecond(540)
    {
      quark::Loggy::Init();
      QK_CORE_INFO("Started logging system.");
      Window::InitLuaBindings();
      m_Timers.push_back(Timer());
      m_Timers.push_back(Timer());
      m_Timers.push_back(Timer());
    }

    Application::~Application()
    {
    }

    void Application::initSystem()
    {
      _name = getDefaultName();
      _properties = getDefaultWindowProperties();
      _window = new Window(this->getName(), this->getDefaultWindowProperties());
      _renderer = new Renderer();
      _renderer->setWindow(_window);
      systems.add<RenderSystem>(_renderer);

      systems.configure();
    }

    void Application::start()
    {
      QK_CORE_INFO("Starting Quark application: ", this->getName());
      this->initSystem();
      this->init();

      QK_CORE_TRACE("Dispatching logic thread");
      std::thread logic_thread(&Application::logicLoop, this);

      QK_CORE_TRACE("Starting main loop");
      while (!_window->isClosed()) {
        update();
      }

      logic_thread.join();
    }

    void Application::logicLoop()
    {
      while (!_window->isClosed()) {
        if (m_Timers[1].getCurrentValue() > FrameTimeFromFrequency<Unit::s>(m_MaxTicksPerSecond)) {
          this->onTick();
          m_Timers[1].softReset();
        }
      }
    }

    void Application::core_debug()
    {
      static float clear_color[] = {0.0, 0.0, 0.0, 1.0};
      static float frametimes[500];
      static int framecursor = 0;
      ImGui::Begin("Quark debugging.");
      if (framecursor > 499) framecursor = 0;
      frametimes[framecursor++] = FrameTimeFromFrequency<Unit::ms>(ImGui::GetIO().Framerate);
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                  FrameTimeFromFrequency<Unit::ms>(ImGui::GetIO().Framerate),
                  ImGui::GetIO().Framerate);
      ImGui::PlotLines("FrameTime", frametimes, 500);
      ImGui::PlotLines("Sin", [](void *data, int idx) { return sinf(idx * 0.2f); }, NULL, 500);
      ImGui::ColorEdit4("Window::clearColor", clear_color);
      ImGui::End();

      _window->setClearColor(clear_color[0], clear_color[2], clear_color[1]);
      Script gui_test_script("./res/scripts/CoreDebugUI.lua");
    }

    void Application::update()
    {
      for (Timer &timer : m_Timers) {
        timer.update();
      }

      if (m_Timers[0].getCurrentValue() > FrameTimeFromFrequency<Unit::s>(m_GUI_MaxFramesPerSecond)) {
        _window->clear();
        core_debug();
        debug();
        systems.update_all(0.1);

        ui();
        ImGui::Render();
        ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
        _window->update();
        m_Timers[0].softReset();
      }
    }
}
