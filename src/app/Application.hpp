#pragma once

#include "app/Window.hpp"
#include "graphics/Renderer.hpp"
#include "utils/utils.hpp"
#include "graphics/Renderable2D.hpp"
#include <memory>

#include "systems/RenderSystem.h"

namespace ex = entityx;

namespace quark
{
    class Application : public ex::EntityX
    {
    public:
        Application();

        virtual ~Application();

        virtual void init() = 0;

    public:
        void start();

        void suspend();

        void resume();

        void logicLoop();

        void stop();

        virtual void onTick() {}

        void initSystem();

        virtual void debug() {}

        virtual void core_debug();

        virtual void ui() {}

    private:
        void update();

    public:
        std::string getName()
        {
          return _name;
        }

        WindowProperties &getWindowProperties()
        {
          return _properties;
        }

        unsigned getWidth()
        {
          return _width;
        }

        unsigned getHeight()
        {
          return _height;
        }

        void setMaxUPS(float ups)
        {
          m_MaxTicksPerSecond = (unsigned) ups;
        }

        virtual const WindowProperties getDefaultWindowProperties() = 0;

        virtual const std::string getDefaultName() = 0;

    protected:
        Renderer *_renderer;

    private:
        Window *_window;
        WindowProperties _properties;

        std::string _name;

        unsigned _width;
        unsigned _height;

        unsigned m_GUI_MaxFramesPerSecond;
        unsigned m_MaxTicksPerSecond;
        std::vector<Timer> m_Timers;
    };
}
