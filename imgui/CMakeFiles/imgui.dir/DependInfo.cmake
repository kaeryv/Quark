# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kaeryv/Projects/Chip8/quark/imgui/imgui.cpp" "/home/kaeryv/Projects/Chip8/quark/imgui/CMakeFiles/imgui.dir/imgui.cpp.o"
  "/home/kaeryv/Projects/Chip8/quark/imgui/imgui_draw.cpp" "/home/kaeryv/Projects/Chip8/quark/imgui/CMakeFiles/imgui.dir/imgui_draw.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "quark/src/public"
  "quark/src"
  "quark/imgui"
  "quark/imgui/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
